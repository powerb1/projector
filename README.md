#Code for parsing paddock modelling output into TRUII's P2R Projector.

For sugarcane:

1. getMetaData.R - creates climate classifications for each catchment (3 in WT and MW, 2  in BM and 1 in BU) and TRUII id codes (see below). Requires access to primary/RegionsRC9/Met/ (see https://bitbucket.org/landresourceapsim/primary).

2. getTRUIIdata.R - For each region compiles ratoon aggregated output into compressed RDS files in output/. Requires access to output files.

3. plotTRUIIdata.R - uses RDS files from 2. to generate plots of each constituent by soil/met/KS combo by management class by zone.

4. writeTRUIIformat.R - loads RDS files from 2. calcs spatial mean for each met into zones and save in format for Projector (ie Id ~ P2Rcode). Saves zipped csv.

5. checkOutput.R - merges with output from 1. (ie data/metaData.csv) and checks/plots output from 4. (ie output/sugarcane.csv)

Notes

TRUII Id 1.23456

where

1 = commodity (e.g. sugarcane)

2 = catchment (i.e. Wet Tropics, Mackay Whitsundays, Burnett, Burdekin)

3 = subregion (e.g. Plane Creek, Herbert)

4 = climate zone (wet, dry)

56 = soil type and permeability

7 = constituent (1 = soil_loss,2 = DINrunoff, 3 = DINleached", 4 = DIN (runoff + leached) ,5 = Diuron equivalent toxic load)

See file data/metaData.csv for TRUII Id code definitions (column TRUIICode)

P2RCode AfBfBfCfDfAf: Soil/tillage = Af, Nutrient = Pest = Bf, Mill mud = Cf, Irrigation = Df, Af = Recycling pit 

There is are no factorial ocombinations of irrigation and no recycling pits in WT and MW and hence corresponding P2RCode = Cf.

The Diuron equivalent toxic load is the sums of Atrazine, Diuron and Hexazinone with TE’s 0.036,1.0,0.21 respectively.




